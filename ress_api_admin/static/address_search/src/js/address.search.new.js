/*---------------------------------------------------
 * Address: Search
 *--------------------------------------------------*/
function initAddressSearchArchiveToggleSwitch() {
    $('#address-search-from-archive-toggle-switch').click(function () {
        if ($(this).is(':checked')) {
            var value = $(this).val();

            $('#address-search-from-archive-headline').removeClass('d-none');
            // DEMO
            showCardPreloader('#tab-address-search-content .card:first');

            // Do some other action

            console.log('#address-search-from-archive-headline: CHECKED');

        } else {

            $('#address-search-from-archive-headline').addClass('d-none');
            // DEMO
            hideCardPreloader('#tab-address-search-content .card:first');

            // Do some other action

            console.log('#address-search-from-archive-headline: NOT CHECKED');
        }
    });
}

/*---------------------------------------------------
 * Init
 *--------------------------------------------------*/
$(function () {
    initAddressSearchArchiveToggleSwitch();
});