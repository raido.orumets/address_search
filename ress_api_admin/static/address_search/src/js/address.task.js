/*---------------------------------------------------
 * Address: Task
 *---------------------------------------------------
 * jQuery validation:
 * - How to translate: https://stackoverflow.com/questions/6181886/how-to-change-jquery-validator-language-message
 * - Jquery Validation and ajax call: https://gist.github.com/jakebellacera/839163
 *--------------------------------------------------*/

function validateAddressTaskForm() {
    var validator = $("#address-task-form").validate({

        rules: {
            address_region: {
                required: true,
                minlength: 3,
            },
            address_city_county: {
                required: true,
                minlength: 3,
            },
            address_municipality: {
                minlength: 2,
            },
            address_street_bg_name: {
                minlength: 2,
            },
            comment_text: {
                minlength: 3,
            },
            sender_name: {
                regex: /^[a-zA-Z\s\-]+$/,
                minlength: 5,
            },
            sender_phone: {
                minlength: 7,
                maxlength: 17,
            },
        },

        messages: {
            sender_name: {
                regex: 'Palun sisesta oma ees- ja perekonnanimi.',
                minlength: 'Palun sisesta oma ees- ja perekonnanimi.',
            },
            address_street_bg_name: {
                minlength: 'See väli on vigaselt täidetud.',
            },
            address_municipality: {
                minlength: 'See väli on vigaselt täidetud.',
            },
            sender_phone: {
                minlength: 'Telefoninumber on vigane.',
                maxlength: 'Telefoninumber on vigane.',
            },
        },

    });

    return validator;
}

function postAddressTask() {

    if ($('#address-task-form').valid()) {
        console.log('#address-task-form deafult validation IS VALID');

        var validator = validateAddressTaskForm();

        var formData = $('#address-task-form').serialize();
        console.log(formData);

        $.ajax({
            method: "POST",
            url: "/post-address-task/",
            data: formData,
            dataType: 'JSON',
            success: function (data) {
                console.log('POST #address-task-form IS SUCCESS');
                console.log(data);
                handlePostAddressTaskSuccess(data, validator);
            },
            error: function (data) {
                console.log('POST #address-task-form HAS ERRORS');
                console.log(data);
                handlePostAddressTaskErrors(data, validator);
            }
        });

    } else {
        console.log('#address-task-form IS NOT VALID');
    }
}

function handlePostAddressTaskSuccess(data, validator) {
    // TODO
}

function handlePostAddressTaskErrors(data, validator) {

    var errors = $.parseJSON(data.responseText).errors;

    if (errors.address_street_bg_name) {
        validator.showErrors({
            'address_street_bg_name': 'Viga raisk!'
        });
    }
}

/*---------------------------------------------------
 * DropZone
 *--------------------------------------------------*/

function initPostAddressTaskDropzone() {
    var dropzone = new Dropzone('#address-task-attachments-dropzone', {
        url: "/", // "/" for fake uploads
        uploadMultiple: true,
        autoProcessQueue: false,
        addRemoveLinks: true,

        acceptedFiles: 'image/jpeg,image/png,image/gif,text/*,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',

        dictDefaultMessage: 'Kliki või tiri failid siia',
        dictFallbackMessage: 'Sinu veebibrauser ei toeta "drag-n-drop" failide üleslaaimist.',
        dictFallbackText: 'Palun kasuta hariliku failide üleslaadimise välja.',
        dictFileTooBig: 'Fail on liiga suure mahuga ({{filesize}}MB). Lubatud maksimaalne faili suurus on {{maxFilesize}}MB.',
        dictInvalidFileType: 'Sellist failitüüpi ei saa üleslaadida.',
        dictResponseError: 'Viga! Server vastas veakoodiga {{statusCode}}.',
        dictCancelUploadConfirmation: 'Oled sa kindel, et soovid katkestada üleslaadimise?',
        dictCancelUpload: 'Katkestan',
        dictMaxFilesExceeded: 'Sa ei saa rohkem faile üleslaadida.',
        dictRemoveFile: 'Eemalda',

        maxFiles: 3,
        maxFilesize: 20,

        accept: function (file) {
            var fileReader = new FileReader();
            fileReader.onload = function (event) {
                imageData = event.target.result;
                addPostAddressTaskAttachmentHiddenInput(file, imageData);
            };
            fileReader.readAsDataURL(file);
        },

        removedfile: function (file) {
            x = confirm('Kas soovid faili eemaldada?');
            if (!x) return false;

            removePostAddressTaskAttachmentHiddenInput(file);
            return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
        },
    });
}

function addPostAddressTaskAttachmentHiddenInput(file, imageData) {
    $('<input type="hidden" name="attachment[]" data-type="' + file.type + '" data-filename="' + file.name + '"  data-uuid="' + file.upload.uuid + '" value="' + imageData + '" />').appendTo('#address-task-attachments-dropzone');
    console.log('Add file: "' + file.name + '" UUUID: ' + file.upload.uuid);
}

function removePostAddressTaskAttachmentHiddenInput(file) {
    $('#address-task-attachments-dropzone input[data-uuid="' + file.upload.uuid + '"]').remove();
    console.log('Remove file: "' + file.name + '" UUUID: ' + file.upload.uuid);
}

/*---------------------------------------------------
 * Preloader
 *--------------------------------------------------*/

function showPostAddressTaskPreloder() {
    showFormButtonPreloader('#location-task-send-btn');
    showCardPreloader('#address-task-form .card');
}

function hidePostAddressTaskPreloder() {
    hideFormButtonPreloader('#location-task-send-btn');
    hideCardPreloader('#address-task-form .card');

    let firstInValidInput = $('#address-task-form .is-invalid:first');
    if (firstInValidInput.length) {
        $('html, body').animate({
            scrollTop: firstInValidInput.offset().top - 110
        }, 800);
        firstInValidInput.focus();
    }
}

function loadAddressTaskSenderParamsFromUrl() {
    // For example: ?tab=address-task&task_sender_name=Mari%20Maasikas&task_sender_email=mari.maasikas%40telia.ee&task_sender_phone=%2B37253123456
    let taskSenderName = getUrlParam('task_sender_name');
    let taskSenderEmail = getUrlParam('task_sender_email');
    let taskSenderPhone = getUrlParam('task_sender_phone');

    let form = $('#address-task-form');
    $('input[name="sender_name"]', form).val(taskSenderName);
    $('input[name="sender_email"]', form).val(taskSenderEmail);
    $('input[name="sender_phone"]', form).val(taskSenderPhone);
}

/*---------------------------------------------------
 * Init
 *--------------------------------------------------*/
$(document).on('click', '#nav-address-task-tab', function () {
    // hideOtherTabsExcept('address-task');
});

$(function () {
    loadAddressTaskSenderParamsFromUrl();
    validateAddressTaskForm();
    initPostAddressTaskDropzone();

    $('#address-task-form').submit(function (e) {
        e.preventDefault();
        postAddressTask();
        // clearFormFields('#address-task-form');
    });

    /* $('#address-task-form input').keyup(function () {
        $("#address-task-form").valid();
    }); */

    /*---------------------------------------------------
     * DEMO
     *--------------------------------------------------*/

    // For demo: Click on the button
    $('#location-task-send-btn').click(function () {
        if (!$(this).prop('disabled')) {
            showPostAddressTaskPreloder();
        }
    });

    // For demo: Click outside of the button
    $('body').on('click', function (event) {
        if ($(event.target).closest('#location-task-send-btn').length === 0) {
            hidePostAddressTaskPreloder();
        }
    });
});