/*---------------------------------------------------
 * Address: Map Coordinates Converter
 *---------------------------------------------------
 * @Author: Märt Elbre
 * @Url:    https://cramer-tl.estpak.ee/cramer/uig/js/coords.js
 *---------------------------------------------------
 * Calculators and manuals:
 * - https://www.benjaminspaulding.com/2015/09/01/long-x-lat-free-illustration-included/
 * - https://coordinates-converter.com/en/
 * - https://gpa.maaamet.ee/calc/geo-lest/
 *---------------------------------------------------
 * X - Latitude - N
 * Y - Longitude - E
 *--------------------------------------------------*/

// TODO: @Raido Need to check (x, y), because it is used another way (y, x)
function lest2geo(x, y) {

    var a = 6378137;
    var F = 1 / 298.257222100883;
    var ESQ = F + F - F * F;
    var B0 = (57 + 31 / 60 + 3.194148 / 3600) / 57.2957795130823;
    var L0 = 24 / 57.2957795130823;
    var FN = 6375000;
    var FE = 500000;
    var B2 = (59 + 20 / 60) / 57.2957795130823;
    var B1 = 58 / 57.2957795130823;
    var xx = x - FN;
    var yy = y - FE;
    var t0 = Math.sqrt((1 - Math.sin(B0)) / (1 + Math.sin(B0)) * Math.pow(((1 + Math.sqrt(ESQ) * Math.sin(B0)) / (1 - Math.sqrt(ESQ) * Math.sin(B0))), Math.sqrt(ESQ)));
    var t1 = Math.sqrt((1 - Math.sin(B1)) / (1 + Math.sin(B1)) * Math.pow(((1 + Math.sqrt(ESQ) * Math.sin(B1)) / (1 - Math.sqrt(ESQ) * Math.sin(B1))), Math.sqrt(ESQ)));
    var t2 = Math.sqrt((1 - Math.sin(B2)) / (1 + Math.sin(B2)) * Math.pow(((1 + Math.sqrt(ESQ) * Math.sin(B2)) / (1 - Math.sqrt(ESQ) * Math.sin(B2))), Math.sqrt(ESQ)));
    var m1 = (Math.cos(B1) / Math.pow((1 - ESQ * Math.sin(B1) * Math.sin(B1)), 0.5));
    var m2 = (Math.cos(B2) / Math.pow((1 - ESQ * Math.sin(B2) * Math.sin(B2)), 0.5));
    var n1 = (Math.log(m1) - Math.log(m2)) / (Math.log(t1) - Math.log(t2));
    var FF = (m1 / (n1 * Math.pow(t1, n1)));
    var p0 = (a * FF * Math.pow(t0, n1));
    var p = Math.pow((yy * yy + (p0 - xx) * (p0 - xx)), 0.5);
    var t = Math.pow((p / (a * FF)), (1 / n1));
    var FII = Math.atan(yy / (p0 - xx));
    var LON = (FII / n1 + L0);
    var u = ((Math.PI / 2) - (2 * Math.atan(t)));
    var LAT = (u + (ESQ / 2 + (5 * Math.pow(ESQ, 2) / 24) + (Math.pow(ESQ, 3) / 12) + (13 * Math.pow(ESQ, 4) / 360)) * Math.sin(2 * u) + ((7 * Math.pow(ESQ, 2) / 48) + (29 * Math.pow(ESQ, 3) / 240) + (811 * Math.pow(ESQ, 4) / 11520)) * Math.sin(4 * u) + ((7 * Math.pow(ESQ, 3) / 120) + (81 * Math.pow(ESQ, 4) / 1120)) * Math.sin(6 * u) + (4279 * Math.pow(ESQ, 4) / 161280) * Math.sin(8 * u));

    LAT = LAT * 57.2957795130823;
    LON = LON * 57.2957795130823;

    var coords = new Array();
    coords['lat'] = LAT;
    coords['lon'] = LON;

    return coords;
}

function retDMS(lat, lon) {

    var lat = lat;
    var lon = lon;
    var latResult, lonResult, dmsResult;

    lat = parseFloat(lat);
    lon = parseFloat(lon);
    latResult = (lat >= 0) ? 'N' : 'S';
    latResult += getDMS(lat);
    lonResult = (lon >= 0) ? 'E' : 'W';
    lonResult += getDMS(lon);
    dmsResult = latResult + ',' + lonResult;

    return dmsResult;
}

function getDMS(val) {

    var valDeg, valMin, valSec, result;

    val = Math.abs(val);
    valDeg = Math.floor(val);
    result = valDeg + "Âº";
    valMin = Math.floor((val - valDeg) * 60);
    result += valMin + "'";
    valSec = Math.round((val - valDeg - valMin / 60) * 3600 * 1000) / 1000;
    result += valSec + '"';

    return result;
}

function showGEO(x, y, item) {

    var valLest = x + "," + y;
    var resLest = "L-EST:<br>" + "(N,E) " + valLest;
    var coords = lest2geo(y, x);
    var valDec = coords.lat + "," + coords.lon;

    var resDec = "WGS84 tÃ¤isarv:<br>" + "(N,E) " + valDec;
    var valDeg = retDMS(coords.lat, coords.lon);

    var resDeg = "WGS84 kraadid:<br>" + "(N,E) " + valDeg;
    var resMapLink = "<a href=\"javascript: window.open('http://www.google.com/maps/place/" + coords.lat + "," + coords.lon + "','_blank');\">Vaata kaardile</a>";

    document.getElementById(item).innerHTML = resLest + "<br>" + resDec + "<br>" + resDeg + "<br>" + resMapLink;
}