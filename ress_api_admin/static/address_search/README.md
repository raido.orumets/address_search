# address_search

## Install

To install all the dependencies ("./node_modules") just run:

```sh
yarn
```

... or ...

```sh
npm install
```

## Development

Adress search files are separated to **2 folders**:

- **"dist"** - Final compiled files, generated automatically. Use thes files in HTML.

- **"src"** - Uncompiled development files. All the development and source files should be in this folder.

To compile files in development, use one time compile command ...

```sh
yarn run dev
```

```sh
yarn run development
```

... or to compile continously ...

```sh
yarn run watch
```

## Production / Deploy

Run this command in production environment:

```sh
yarn run production
```

# JS modules

## Grid JS

Documentation: https://gridjs.io/docs/index

Include css and js files:

```html
<!-- Drid JS CSS -->
<link rel="stylesheet" type="text/css" href="https://unpkg.com/gridjs/dist/theme/mermaid.min.css" />

<!-- Grid JS -->
<script src="static/address_search/dist/vendor/gridjs/gridjs.production.min.js" type="text/javascript"></script>
<script src="static/address_search/dist/vendor/gridjs-jquery/gridjs.production.min.js" type="text/javascript"></script>
```

Usage:

```javascript
// jQuery
$("#tab-address-documents-table").Grid({
    // ...
});

// VanillaJS
new gridjs.Grid({
    // ...
}).render(document.getElementById("tab-address-documents-table"));

// jQuery and VanillaJS combo
new gridjs.Grid({
    // ...
}).render($("#tab-address-documents-table")[0]);
```


## Jquery Inline SVG

https://www.npmjs.com/package/jquery-inline-svg

## Geometric Polygon Example

```xml
<gml:Polygon srsName="EPSG:3301">
    <gml:outerBoundaryIs>
        <gml:LinearRing>
            <gml:coordinates>404094.2,6492293.6 404102.7,6492302.5 404158.3,6492329.1 404212,6492353.9 404257.3,6492374.3 404470,6492180.4 404332.4,6492032.5 404075.4,6492273.4 404094.2,6492293.6</gml:coordinates>
        </gml:LinearRing>
    </gml:outerBoundaryIs>
</gml:Polygon>
```