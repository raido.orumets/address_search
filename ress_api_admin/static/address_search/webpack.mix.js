/*---------------------------------------------------
 * NB! Only for development use or downloading and
 * copying node modules / packages. Development can
 * be done without webpack also - it is your choice.
 *--------------------------------------------------*/

const mix = require('laravel-mix');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const ImageminMozjpeg = require('imagemin-mozjpeg');
const CopyPlugin = require('copy-webpack-plugin');

mix.options({
    processCssUrls: false
});

//mix.setPublicPath('dist');

if (mix.inProduction()) {

    mix.version();
    mix.disableNotifications();

} else {

    /* mix.browserSync({
        proxy: 'http://localhost:8001'
    }); */

    // Fixto
    mix.copy('node_modules/fixto/dist/fixto.min.js', 'src/vendor/fixto/fixto.min.js');
    // Bootstrap
    mix.copy('node_modules/bootstrap/dist/css/bootstrap.min.css', 'src/vendor/bootstrap/bootstrap.min.css');
    mix.copy('node_modules/bootstrap/dist/css/bootstrap.min.css.map', 'src/vendor/bootstrap/bootstrap.min.css.map');
    mix.copy('node_modules/bootstrap/dist/js/bootstrap.min.js', 'src/vendor/bootstrap/bootstrap.min.js');
    mix.copy('node_modules/bootstrap/dist/js/bootstrap.min.js.map', 'src/vendor/bootstrap/bootstrap.min.js.map');
    mix.copy('node_modules/bootstrap/dist/js/bootstrap.bundle.min.js', 'src/vendor/bootstrap/bootstrap.bundle.min.js');
    mix.copy('node_modules/bootstrap/dist/js/bootstrap.bundle.min.js.map', 'src/vendor/bootstrap/bootstrap.bundle.min.js.map');
    // Popper
    mix.copy('node_modules/popper.js/dist/umd/popper.min.js', 'src/vendor/popper.js/popper.min.js');
    mix.copy('node_modules/popper.js/dist/umd/popper.min.js.map', 'src/vendor/popper.js/popper.min.js.map');
    // Bootstrap: Confirmation
    mix.copy('node_modules/bootstrap-confirmation2/dist/bootstrap-confirmation.js', 'src/vendor/bootstrap-confirmation2/bootstrap-confirmation.js');
    mix.copy('node_modules/bootstrap-confirmation2/dist/bootstrap-confirmation.js.map', 'src/vendor/bootstrap-confirmation2/bootstrap-confirmation.js.map');
    // jQuery
    mix.copy('node_modules/jquery/dist/jquery.min.js', 'src/vendor/jquery/jquery.min.js');
    mix.copy('node_modules/jquery/dist/jquery.min.map', 'src/vendor/jquery/jquery.min.map');
    // jQuery Validation
    mix.copy('node_modules/jquery-validation/dist/jquery.validate.min.js', 'src/vendor/jquery-validation/jquery.validate.min.js');
    mix.copy('node_modules/jquery-validation/dist/localization/messages_et.min.js', 'src/vendor/jquery-validation/localization/messages_et.min.js');
    mix.copy('node_modules/jquery-validation/dist/localization/messages_uk.min.js', 'src/vendor/jquery-validation/localization/messages_uk.min.js');
    // jQuery Inline SVG
    mix.copy('node_modules/jquery-inline-svg/dist/jquery-inline-svg.min.js', 'src/vendor/jquery-inline-svg/jquery-inline-svg.min.js');
    // Dropzone
    mix.copy('node_modules/dropzone/dist/dropzone-min.js', 'src/vendor/dropzone/dropzone-min.js');
    mix.copy('node_modules/dropzone/dist/dropzone-min.js.map', 'src/vendor/dropzone/dropzone-min.js.map');
    mix.copy('node_modules/dropzone/dist/dropzone.css', 'src/vendor/dropzone/dropzone.css');
    mix.copy('node_modules/dropzone/dist/dropzone.css.map', 'src/vendor/dropzone/dropzone.css.map');
    // Grid.js - https://gridjs.io/docs/integrations/jquery
    mix.copy('node_modules/gridjs/dist/gridjs.production.min.js', 'src/vendor/gridjs/gridjs.production.min.js');
    mix.copy('node_modules/gridjs/dist/gridjs.production.min.js.map', 'src/vendor/gridjs/gridjs.production.min.js.map');
    mix.copy('node_modules/gridjs-jquery/dist/gridjs.production.min.js', 'src/vendor/gridjs-jquery/gridjs.production.min.js');
    mix.copy('node_modules/gridjs-jquery/dist/gridjs.production.min.js.map', 'src/vendor/gridjs-jquery/gridjs.production.min.js.map');

    mix.copy('../../templates/address_search/index.html', '../../public/index.html');
}

mix.copy('src/css/main.css', 'dist/css/main.css');

mix.sass('src/sass/fonts.scss', 'dist/css/');

mix.sass('src/sass/bootstrap-floating-label.scss', 'src/vendor/bootstrap-floating-label/');
if (mix.inProduction()) {
    mix.minify('src/vendor/bootstrap-floating-label/bootstrap-floating-label.css');
}

mix.copyDirectory('src/js', 'dist/js');
mix.copyDirectory('src/fonts', 'dist/fonts');
mix.copyDirectory('src/vendor', 'dist/vendor');

/* mix.babel([
    'src/js/app.js'
], 'dist/js/app.js'); */

mix.webpackConfig({
    plugins: [
        new CopyPlugin({
            patterns: [{
                from: 'src/images', // 'src/imges'
                to: 'dist/images' // if mix.setPublicPath('dist') is set, then just 'images'
            }],
        }),
        new ImageminPlugin({
            test: /\.(jpe?g|png|gif|svg)$/i,
            plugins: [
                ImageminMozjpeg({
                    quality: 65,
                })
            ]
        }),
    ]
});

// Full API
// mix.js(src, output);
// mix.react(src, output); <-- Identical to mix.js(), but registers React Babel compilation.
// mix.extract(vendorLibs);
// mix.sass(src, output);
// mix.standaloneSass('src', output); <-- Faster, but isolated from Webpack.
// mix.fastSass('src', output); <-- Alias for mix.standaloneSass().
// mix.less(src, output);
// mix.stylus(src, output);
// mix.postCss(src, output, [require('postcss-some-plugin')()]);
// mix.browserSync('my-site.dev');
// mix.combine(files, destination);
// mix.babel(files, destination); <-- Identical to mix.combine(), but also includes Babel compilation.
// mix.copy(from, to);
// mix.copyDirectory(fromDir, toDir);
// mix.minify(file);
// mix.sourceMaps(); // Enable sourcemaps
// mix.version(); // Enable versioning.
// mix.disableNotifications();
// mix.setPublicPath('path/to/public');
// mix.setResourceRoot('prefix/for/resource/locators');
// mix.autoload({}); <-- Will be passed to Webpack's ProvidePlugin.
// mix.webpackConfig({}); <-- Override webpack.config.js, without editing the file directly.
// mix.then(function () {}) <-- Will be triggered each time Webpack finishes building.
// mix.options({
//   extractVueStyles: false, // Extract .vue component styling to file, rather than inline.
//   processCssUrls: true, // Process/optimize relative stylesheet url()'s. Set to false, if you don't want them touched.
//   purifyCss: false, // Remove unused CSS selectors.
//   uglify: {}, // Uglify-specific options. https://webpack.github.io/docs/list-of-plugins.html#uglifyjsplugin
//   postCss: [] // Post-CSS options: https://github.com/postcss/postcss/blob/master/docs/plugins.md
// });